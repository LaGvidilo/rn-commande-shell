/*
 *  copy.h
 *  rn
 *
 *  Created by Rick Sanchez on 12/04/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */
#include <iostream>
#include <stdio.h>
#include <fstream>
using namespace std;



void ren(string A, string B){
		std::ifstream is (A.c_str(), std::ifstream::binary);
		if (is) {
			is.seekg (0, is.end);
			long length = (long)is.tellg();
			is.seekg (0, is.beg);
			char * buffer = new char [length];
			is.read (buffer,length);
			if (is)
				std::cout << "all characters read successfully.";
			else
				std::cout << "error: only " << is.gcount() << " could be read";
			is.close();
			
			std::fstream fs;
			fs.open (B.c_str(), std::fstream::out);
			
			fs << buffer;
			
			fs.close();
			
			delete[] buffer;
			remove(A.c_str());
		}
}