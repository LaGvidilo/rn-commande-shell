#include <iostream>
#include "copy.h"
using namespace std;
void usage(){
	cout << "rn v1 by rick sanchez\nUsage for rename file: rn <source_file> <target_file>\n";
}


int main (int argc, char * const argv[]) {
	if (argc==3){
		
		ren((string)argv[1],(string)argv[2]);
		
	}
	else{
		usage();
	}
    return 0;
}
